<?php
  require 'config.php';
  require 'Transaction.php';
  class App {
    private $auth;
    public function __construct() {
      global $config; // Load $config variable //
      $this->url = $config['url'];

      $this->auth = $config['auth']; // Asign Auth Array from Config in $this->auth//
    }

  // !!! This Method is Private, you can call this method only in the public method of this class // !!!
  private function connect() {

    $this->request = http_build_query($this->auth); // Preparing Array for CURL //
    $ch = curl_init(); // Initializing CURL //

    // CURL SETTINGS //
    curl_setopt($ch, CURLOPT_URL, $this->url); // Request URL //
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $this->request); // Send POST Parameters (from $config['auth']) //
    curl_setopt($ch, CURLOPT_POST, 1); // Enable Post request //
  // //

    $result = curl_exec($ch); // Execute and Store result //

    // Error Handling //
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    //

    curl_close ($ch); // Close CURL call //
    return json_decode($result); // Calling "connect" method will return the data //

  }

    public function getData() {
      return $this->connect(); // get All Data //
    }

    // Get All Transactions // use this for fetching raw data //
    public function getTransactions() {
      $data = $this->getData();
      return $data->transactions; // return only transactions //
    }

    // all transactions stored in Transaction Class //
    public function loopTransactions() {
      $data = $this->getTransactions();
      $transactions = array();
      foreach($data as $single) {
        $transactions[] = new Transaction($single);
      }
      return $transactions;
    }
    // Get Single Transaction by ID //
    public function getTransaction($id) {
      $data = $this->getTransactions(); // Store All Transactions//

      // Filter Data, and return only Transaction with the requested ID //
      foreach($data as $single) {
        if($single->_id == $id) {
          return new Transaction($single);
        }
      }

    }

  }
