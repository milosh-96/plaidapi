<?php
  class Transaction {
    public function __construct($data) {
      $noDataVar = "No Data";
      $this->id = $data->_id;
      $this->account = $data->_account;
      $this->amount = $data->amount;
      $this->date = $data->date;
      $this->name = $data->name;

      if(count($data->meta->location) > 0) {
        $location = $data->meta->location;
        isset($location->city) ? $this->city = $location->city : $this->city = $noDataVar;
        isset($location->state) ? $this->state = $location->state : $this->state = $noDataVar;
        isset($location->address) ? $this->address = $location->address : $this->address = $noDataVar;
      }

      $this->pending = $data->pending;

      if(count($data->type) > 0) {
        $type = $data->type;

        isset($type) ? $this->primaryType = $type->primary : $this->primaryType = $noDataVar;
      }
      $this->category = $data->category;
  }
}
